
const baseurl = 'https://cors-anywhere.herokuapp.com/http://mynukad.com/Nukkad/nukadmanager.php/nukad';

export function PostData(formdata) {
    console.log(formdata)
    return new Promise((resolve, reject) => {
        fetch(`${baseurl}/getuser`, {
            method: 'POST',
            headers: {
                "access-control-allow-origin" : "*",
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(formdata)
        })
            .then((Response) => Response.json())
            .then((responseJson) => {
                resolve(responseJson);
                console.log("User Name :",responseJson);
            })
            
            .catch((error) => {
                reject(error);
            })
            // .then(function (Response) {
            //     console.log("hh",Response);
            //   })
            //   .catch(function (error) {
            //     console.log(error);
            //   });
    });
}


export function PostDataTATAapi(postformdata) {
    console.log(postformdata)
    return new Promise((resolve, reject) => {
        fetch(`https://cors-anywhere.herokuapp.com/https://api-in21.leadsquared.com/v2/LeadManagement.svc/Lead.Capture?accessKey=u$r271f6a158e8cb28bf07c41806128121c&secretKey=f8524c520a4f2324523c122eb8ec1d016060b8fb`, {
            method: 'POST',
            headers: {
                "access-control-allow-origin" : "*",
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(postformdata)
        })
            .then((Response) => Response.json())
            .then((responseJson) => {
                resolve(responseJson);
                console.log("User Name :",responseJson);
            })
            
            .catch((error) => {
                reject(error);
            })
            // .then(function (Response) {
            //     console.log("hh",Response);
            //   })
            //   .catch(function (error) {
            //     console.log(error);
            //   });
    });
}



export function mynukadPostData(postmynukad) {
    console.log(postmynukad)
    return new Promise((resolve, reject) => {
        fetch(`https://cors-anywhere.herokuapp.com/https://mynukadapi.herokuapp.com/api/products`, {
            method: 'POST',
            headers: {
                "access-control-allow-origin" : "*",
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(postmynukad)
        })
            .then((Response) => Response.json())
            .then((responseJson) => {
                resolve(responseJson);
                console.log("User Name :",responseJson);
            })
            
            .catch((error) => {
                reject(error);
            })
            // .then(function (Response) {
            //     console.log("hh",Response);
            //   })
            //   .catch(function (error) {
            //     console.log(error);
            //   });
    });
}


export function mynukadGETData(postmynukad) {
    console.log(postmynukad)
    return new Promise((resolve, reject) => {
        fetch(`https://cors-anywhere.herokuapp.com/https://mynukadapi.herokuapp.com/api/products`, {
            method: 'GET',
            headers: {
                "access-control-allow-origin" : "*",
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(postmynukad)
        })
            .then((Response) => Response.json())
            .then((responseJson) => {
                resolve(responseJson);
                console.log("User Name :",responseJson);
            })
            
            .catch((error) => {
                reject(error);
            })
            // .then(function (Response) {
            //     console.log("hh",Response);
            //   })
            //   .catch(function (error) {
            //     console.log(error);
            //   });
    });
}