import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'
import './style1.css'
import {mynukadGETData} from './Api'
import swal from 'sweetalert';
import logoimg from './img/logo.png'
import {MDBDataTable} from 'mdbreact'

export default class view extends Component {

    constructor(props) {

        super(props);
        this.state = {
            Items: [],
        }
    }

    componentDidMount() {
        mynukadGETData()
        .then((responseJson, error) => {
            
            this.setState({
                Items:responseJson.response
            })
            console.log(this.state.Items, "UserID")
    })
    
  }
  
    render(){console.log(this.state.Items, "hhff")
        var {Items}= this.state;

        const data = {
            columns: [
                {
                    label: 'ID',
                    field: 'PersonID',
                    sort: 'asc',
                    width: 150
                },
                {
                    label: 'NAME',
                    field: 'FirstName',
                    sort: 'asc',
                    width: 150
                },
                {
                    label: 'Email',
                    field: 'EmailAddress',
                    sort: 'asc',
                    width: 150
                },
                {
                    label: 'MOBILE',
                    field: 'Phone',
                    sort: 'asc',
                    width: 150
                },
                {
                    label: 'SOCIETY',
                    field: 'mx_Society_Name',
                    sort: 'asc',
                    width: 150
                },
                {
                    label: 'CITY',
                    field: 'mx_Broadband_City',
                    sort: 'asc',
                    width: 200
                },
                {
                    label: 'STATE',
                    field: 'mx_Broadband_State',
                    sort: 'asc',
                    width: 200
                },
                {
                    label: 'PINCODE',
                    field: 'mx_Pincode',
                    sort: 'asc',
                    width: 150
                },

                {
                    label: 'SOURACE',
                    field: 'SSource',
                    sort: 'asc',
                    width: 200
                },

            ],

            rows: [ ...this.state.Items.map((data, i) => (

                {
                   

                    PersonID:data.PersonID,
    FirstName:data.FirstName ,
    Phone:data.Phone,  
    EmailAddress:data.EmailAddress,
    SSource:data.SSource,
    mx_Broadband_City:data.mx_Broadband_City,
    mx_Broadband_State:data.mx_Broadband_State, 
    mx_Pincode:data.mx_Pincode ,
    mx_Society_Name:data.mx_Society_Name
                        
                }

            )) ]
        }
        console.log("aa",data)
        return(
            <div>
                <div className="row">
                        <div className="col">
                            <div className="employee_dtls">

                                <MDBDataTable
                                    striped
                                    bordered
                                    noBottomColumns
                                    hover
                                    responsive
                                    paging={false}
                                    // searching={false}
                                    data={data}
                                /></div>
                                </div></div>
            </div>
        )
    }
}